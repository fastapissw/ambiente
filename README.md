# ambiente FastAPI - MongoDB - MongoDBExpress
Essa simples aplicação é pra mostrar como usar o FastAPI, acessando de forma simples e rápida, dados cadastrados no MongoDB. 
Você poderá usar o MongoExpress para poder ver as suas coleções e manipular.

Esa aplicação está usando:
- fastapi>=0.68.0,<0.69.0
- pydantic>=1.8.0,<2.0.0
- uvicorn>=0.15.0,<0.16.0
- python-multipart>=0.0.5
- Jinja2>=3.0.3
- SQLAlchemy>=1.4.27
- tenacity>=8.0.1
- httpx>=0.21.1
- passlib>=1.7.4
- python-jose>=3.3.0
- pymongo>=3.11.1
- motor>=2.5.1
- python-decouple>=3.5
- email-validator>=1.1.3
- pytest>=6.2.5
- requests>=2.26.0
- PyJWT>=2.3.0

Nessa aplicação tem também uma parte de tests usando o PyTest, PyJWT.

- Docker
- Docker Compose
- FastAPI
- MongoDB
- MongoExpress
- Tests (PyTest)
- JWT (PyJWT)
- Api Rest

-api
    |----server
        |---auth
            |----auth_bearer.py
            |----auth_handler.py
        |---databases
            |----film.py
            |----planet.py
            |----user.py
        |---models
            |----films.py
            |----planet.py
            |----user.py
        |---routes
            |----films.py
            |----planet.py
            |----user.py
        |---tests
            |----test_app.py
            |----test_film.py
            |----test_planet.py
        ----app.py
    --Dockerfile
    --main.py
    --requirements.txt

## Baixando o projeto

Para baixar o projeto basta copiar a linha e executar: git clone --recursive https://gitlab.com/fastapissw/ambiente.git

Isso vai baixar o projeto e o seu sub módulo [API] (https://gitlab.com/fastapissw/api.git)

## Subindo o ambiente
- Parasubir o ambiente copie a linha e execute: docker-compose up --build

Isso vai baixar as imagens do fastapi, mongodb e mongo-express e instalar todas as dependências necessárias para testar a applicação.

- API: http://0.0.0.0:8001
- MongoDB: host: 0.0.0.0 / port: 27017
- MongoExpress: http://0.0.0.0:8081
- Swagger: http://0.0.0.0:8001/docs